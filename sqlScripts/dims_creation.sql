drop table if exists dimension_db.ref_account;
create table dimension_db.ref_account(
  account_id int,
  tenant_id int,
  tenant_sub_id int,
  company_id smallint,
  account_type_id tinyint,
  account string
);

drop table if exists dimension_db.ref_account_type;
create table dimension_db.ref_account_type(
  account_type_id tinyint,
  tenant_id int,
  tenant_sub_id int,
  account_type string);

drop table if exists dimension_db.ref_call_char;
create table dimension_db.ref_call_char(
  call_char_id tinyint,
  tenant_id int,
  tenant_sub_id int,
  call_char string
);

drop table if exists dimension_db.ref_call_type;
create table dimension_db.ref_call_type(
  call_type_id tinyint,
  tenant_id int,
  tenant_sub_id int,
  call_type string
);

drop table if exists dimension_db.ref_company;
create table dimension_db.ref_company(
  company_id smallint,
  tenant_id int,
  tenant_sub_id int,
  company string);

drop table if exists dimension_db.ref_country;
create table dimension_db.ref_country(
  country_id int,
  tenant_id int,
  tenant_sub_id int,
  region_id int,
  country_code_iso3 string,
  country_code string,
  country string);

drop table if exists dimension_db.ref_currency;
create table dimension_db.ref_currency(
  currency_id int,
  tenant_id int,
  tenant_sub_id int,
  currency_code_iso string,
  currency_symbol string,
  currency string);

drop table if exists dimension_db.ref_destination_type;
create table dimension_db.ref_destination_type(
  destination_type_id int,
  tenant_id int,
  tenant_sub_id int,
  destination_type string);

drop table if exists dimension_db.ref_destination;
create table dimension_db.ref_destination(
  destination_id int,
  tenant_id int,
  tenant_sub_id int,
  destination_type_id int,
  number_plan_id int,
  country_id int,
  region_id int,
  call_char_id int,
  destination string);

drop table if exists dimension_db.ref_direction;
create table dimension_db.ref_direction(
  direction_id int,
  tenant_id int,
  tenant_sub_id int,
  direction string);

drop table if exists dimension_db.ref_fault_code_category;
create table dimension_db.ref_fault_code_category(
  fault_code_category_id int,
  tenant_id int,
  tenant_sub_id int,
  fault_code_category string);

drop table if exists dimension_db.ref_fault_code;
create table dimension_db.ref_fault_code(
  fault_code_id int,
  tenant_id int,
  tenant_sub_id int,
  fault_code_category_id int,
  fault_code int,
  fault_code_dscr string);

drop table if exists dimension_db.ref_region;
create table dimension_db.ref_region(
  region_id int,
  tenant_id int,
  tenant_sub_id int,
  region_code string,
  region string);

drop table if exists dimension_db.ref_route_class;
create table dimension_db.ref_route_class(
  route_class_id int,
  tenant_id int,
  tenant_sub_id int,
  route_class_code string,
  route_class string);

drop table if exists dimension_db.ref_service_level;
create table dimension_db.ref_service_level(
  service_level_id int,
  tenant_id int,
  tenant_sub_id int,
  route_class_id int,
  service_level_code string,
  service_level string);

drop table if exists dimension_db.ref_service;
create table dimension_db.ref_service(
  service_id int,
  tenant_id int,
  tenant_sub_id int,
  service_code string,
  service string);

drop table if exists dimension_db.ref_switch_type;
create table dimension_db.ref_switch_type(
  switch_type_id int,
  tenant_id int,
  tenant_sub_id int,
  switch_type string);

drop table if exists dimension_db.ref_switch;
create table dimension_db.ref_switch(
  switch_id int,
  tenant_id int,
  tenant_sub_id int,
  switch_type_id int,
  switch string);

drop table if exists dimension_db.ref_transmission_type;
create table dimension_db.ref_transmission_type(
  transmission_type_id int,
  tenant_id int,
  tenant_sub_id int,
  transmission_type string);

drop table if exists dimension_db.ref_trunk_type;
create table dimension_db.ref_trunk_type(
  trunk_type_id int,
  tenant_id int,
  tenant_sub_id int,
  trunk_type string);

drop table if exists dimension_db.ref_trunk;
create table dimension_db.ref_trunk(
  trunk_id int,
  tenant_id int,
  tenant_sub_id int,
  trunk_type_id int,
  transmission_type_id int,
  switch_id int,
  account_id int,
  trunk string);

drop table if exists dimension_db.ref_user;
create table dimension_db.ref_user(
  user_id int,
  tenant_id int,
  tenant_sub_id int,
  username string,
  first_name string,
  last_name string
);